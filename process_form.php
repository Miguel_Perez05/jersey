<?php
//PROCESS NEWSLETTER FORM HERE

if(!isset($_POST) || !isset($_POST['email']))
{ 
    $msg = 'No data has been received.';
    $msg = 'No se ha recibido su información.';
    echo '<div class="alert alert-danger"><p><i class="fa fa-exclamation-triangle"></i> '.$msg.'</p></div>';
    return false;
}

if($_POST['email'] == '')
{
    //ERROR: FIELD "email" EMPTY
    $msg = 'Please enter a valid email.';
    $msg = 'Ingrese un email válido.';
    echo '<div class="alert alert-danger"><p><i class="fa fa-exclamation-triangle"></i> '.$msg.'</p></div>';
    return false;
}

///////////////////////////////////////////////
//Now yo can save subscriber in your database//
//And send confirmation email if necessary...//
///////////////////////////////////////////////

//Option 1) Send confirmation email. More info here: http://php.net/manual/es/function.mail.php

/*
mail("my_email@exemple.com","New subscriber","Email: ".$_POST['email']);
*/

//Option 2) Save subscriber on TXT file. More info here: http://www.w3schools.com/php/php_file_create.asp

/*
$myfile = fopen("subscribers.txt", "a") or die("Unable to open file!");
$txt = $_POST['email']."\n";
fwrite($myfile, $txt);
fclose($myfile);
*/
$email=$_POST['email'];
$mail = "Correo: $email";
//Titulo
$titulo = "Recepcion desde pagina en construccion";
//cabecera
$headers = "MIME-Version: 1.0\r\n"; 
$headers .= "Content-type: text/html; charset=iso-8859-1\r\n"; 
//dirección del remitente 
$headers .= "From: Geeky Theory < $email >\r\n";
//Enviamos el mensaje a tu_dirección_email 
$bool = mail("ventas@myjersey.com.mx",$titulo,$mail,$headers);
if($bool){
    $msg = 'Su correo ha sido recibido con éxito, espere nuevas noticias.';
    echo '<div class="alert alert-success"><p><i class="fa fa-check"></i> '.$msg.'</p></div>';
    // echo "Mensaje enviado";
}else{
    $msg = 'Error al recibir el correo.';
    echo '<div class="alert alert-success"><p><i class="fa fa-check"></i> '.$msg.'</p></div>';
    // echo "Mensaje no enviado";
}

//And send success message:
// $msg = 'Su correo ha sido recibido con éxito, espere nuevas noticias.';
// echo '<div class="alert alert-success"><p><i class="fa fa-check"></i> '.$msg.'</p></div>';
return true;

?>
